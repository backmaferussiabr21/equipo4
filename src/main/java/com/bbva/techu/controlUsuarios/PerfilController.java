package com.bbva.techu.controlUsuarios;

import com.bbva.techu.controlUsuarios.entity.PerfilDB;
import com.bbva.techu.controlUsuarios.entity.PerfilRepository;
import com.bbva.techu.controlUsuarios.entity.UsuarioDB;
import com.bbva.techu.controlUsuarios.entity.UsuarioRepository;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class PerfilController {

    @Autowired
    private PerfilRepository repository;
    @Autowired
    private UsuarioRepository repositoryUser;

    //GET: Obtiene la lista de todos los Perfiles
    @GetMapping(value="/perfiles", produces = "application/json")
    @ApiOperation(value = "Obtiene una lista de los Perfiles", notes = "Este método obtiene una lista de todos los Perfiles")
    public ResponseEntity<List<PerfilDB>> ObtenerPerfiles(){
        List<PerfilDB> lista=repository.findAll();
        return new ResponseEntity(lista, HttpStatus.OK);
    }

    //GET: Obtiene un perfil
    @GetMapping(value="/perfiles/{idPerfil}", produces = "application/json")
    @ApiOperation(value = "Obtiene un Perfil", notes = "Este método obtiene un Perfil")
    public ResponseEntity<PerfilDB> ObtenerPerfil(@PathVariable String idPerfil){
        Optional<PerfilDB> perfilDB =repository.findById(idPerfil);
        if (perfilDB.isPresent()){
            return new ResponseEntity(perfilDB, HttpStatus.OK);
        }
        return new ResponseEntity("Perfil no encontrado", HttpStatus.NOT_FOUND);
    }

    //POST: Crea un perfil
    @PostMapping(value="/perfiles")
    @ApiOperation(value = "Crear Perfil", notes = "Este método crea Perfiles")
    public ResponseEntity<String> addPerfil(@ApiParam(name = "Usuario",
                                                      type = "Usuario",
                                                      required = true)
                                                      @RequestBody PerfilDB idPerfil){

        PerfilDB resultado = repository.insert(idPerfil);
        return new ResponseEntity<String>(resultado.toString(),HttpStatus.OK);
    }

    //PUT: Actualizar por perfil
    @PutMapping(value = "/perfiles/{idPerfil}")
    @ApiOperation(value = "Actualiza un Perfil", notes = "Este método actualiza un Perfil")
    public ResponseEntity<String> actualizarPerfil(@PathVariable String idPerfil, @RequestBody PerfilDB perfilDB){
        Optional<PerfilDB> resultado = repository.findById(idPerfil);
        if(resultado.isPresent()){
            PerfilDB perfilAModificar = resultado.get();
            perfilAModificar.nombrePerfil = perfilDB.nombrePerfil;
            perfilAModificar.descripcionPerfil = perfilDB.descripcionPerfil;
            PerfilDB saved = repository.save(resultado.get());
            return new ResponseEntity<String>(resultado.toString(),HttpStatus.OK);
        }
        return new ResponseEntity<String>("Perfil a actualizar no encontrado",HttpStatus.NOT_FOUND);
    }

    //DELETE: Eliminar todos
    @DeleteMapping(value = "/perfiles")
    @ApiOperation(value = "Elimina todos los Perfiles", notes = "Este método elimina todos los Perfiles")
    public ResponseEntity<String> deleteAllPerfiles(){
        repository.deleteAll();
        return new ResponseEntity<String>("Se han eliminado todos los perfiles", HttpStatus.OK);
    }

    //DELETE: Eliminar por perfil
    @DeleteMapping(value = "/perfiles/{idPerfil}")
    @ApiOperation(value = "Elimina un Perfil", notes = "Este método elimina un Perfil")
    public ResponseEntity<String> deletePerfil (@PathVariable String idPerfil){
        Optional<PerfilDB> resultado = repository.findById(idPerfil);
        if (resultado.isPresent()){
            repository.deleteById(idPerfil);
            return new ResponseEntity<String>("Perfil " + idPerfil + " eliminado", HttpStatus.OK);
        }
        return new ResponseEntity<String>("Perfil a eliminar no encontrado", HttpStatus.NOT_FOUND);
    }

    //GET: Buscar Usuario por Perfil
    @GetMapping(value = "/perfiles/usuarios/{idPerfil}")
    @ApiOperation(value = "Obtiene un Usuario a partir de un Perfil", notes = "Este método obtiene un Usuario a partir de un Perfil existente")
    public ResponseEntity<List<UsuarioDB>> ObtenerUsuarioporPerfil(@PathVariable String idPerfil){
        Optional<PerfilDB> perfilDB =repository.findById(idPerfil);
        if (perfilDB.isPresent()){
            ResponseEntity<List<UsuarioDB>> resultado = null;
            try {
                List<UsuarioDB> listaUser = repositoryUser.findByPerfil(idPerfil);
                resultado = new ResponseEntity<List<UsuarioDB>>(listaUser, HttpStatus.OK);

            }
            catch (Exception e) {
                resultado = new ResponseEntity<List<UsuarioDB>>(HttpStatus.NOT_FOUND);

            }
            return resultado;
        }
        return new ResponseEntity("Perfil no encontrado", HttpStatus.NOT_FOUND);
    }
}
