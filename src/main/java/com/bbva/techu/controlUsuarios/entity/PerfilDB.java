package com.bbva.techu.controlUsuarios.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("Perfiles")
public class PerfilDB {
    @Id
    public String idPerfil;
    public String nombrePerfil;
    public String descripcionPerfil;

    public PerfilDB(String idPerfil, String nombrePerfil, String descripcionPerfil) {
        this.idPerfil = idPerfil;
        this.nombrePerfil = nombrePerfil;
        this.descripcionPerfil = descripcionPerfil;
    }

    @Override
    public String toString() {
        return "PerfilDB{" +
                "idPerfil=" + idPerfil +
                ", nombrePerfil='" + nombrePerfil + '\'' +
                ", descripcionPerfil='" + descripcionPerfil + '\'' +
                '}';
    }
}
