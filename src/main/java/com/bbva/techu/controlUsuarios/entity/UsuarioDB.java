package com.bbva.techu.controlUsuarios.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("Usuarios")
public class UsuarioDB {

    @Id
    public String usuario;
    public String nombre;
    public String correo;
    public String estado;
    public String idPerfil;

    public UsuarioDB(){

    }

    public UsuarioDB (String usuario,String nombre, String correo, String estado, String idPerfil){
        this.nombre=nombre;
        this.usuario=usuario;
        this.correo=correo;
        this.estado=estado;
        this.idPerfil=idPerfil;
    }

    @Override
    public String toString(){
        return String.format("Usuario[usuario=%s, nombre=%s, correo=%s, estado=%s, idPerfil=%s]", usuario, nombre,  correo,  estado,  idPerfil);
    }
}
