package com.bbva.techu.controlUsuarios.entity;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsuarioRepository extends MongoRepository<UsuarioDB, String> {

    //Busqueda porperfils
    @Query("{'idPerfil': ?0}")
    public List<UsuarioDB> findByPerfil(String idPerfil);

}
