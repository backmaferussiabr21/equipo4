package com.bbva.techu.controlUsuarios;

import com.bbva.techu.controlUsuarios.entity.UsuarioDB;
import com.bbva.techu.controlUsuarios.entity.UsuarioRepository;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class UsuariosController {

    @Autowired
    private UsuarioRepository repository;

    //GET: Obtiene una lista de todos los Usuarios
    @GetMapping(value="/usuarios", produces = "application/json")
    @ApiOperation(value = "Obtiene listado de todos los Usuarios", notes = "Este método obtiene un listado de los Usuarios")
    public ResponseEntity<List<UsuarioDB>> ObtenerListado(){
        List<UsuarioDB> lista=repository.findAll();
        return new ResponseEntity(lista, HttpStatus.OK);
    }

    //GET: Obtiene un Usuario
    @GetMapping(value="/usuarios/{usuario}", produces = "application/json")
    @ApiOperation(value = "Obtiene un Usuario", notes = "Este método obtiene un Usuario")
    public ResponseEntity<UsuarioDB> ObtenerUsuario(@PathVariable String usuario){
        Optional<UsuarioDB> usuarioDB =repository.findById(usuario);
        if (usuarioDB.isPresent()){
            return new ResponseEntity(usuarioDB, HttpStatus.OK);
        }
        return new ResponseEntity("Usuario no encontrado", HttpStatus.NOT_FOUND);
    }

    //POST: Crea un Usuario
    @PostMapping(value="/usuarios")
    @ApiOperation(value = "Crear Usuario", notes = "Este método crea Usuarios")
    public ResponseEntity<String> addUsuario(@ApiParam(name = "Usuario",
                                                       type = "Usuario",
                                                       required = true)
                                                       @RequestBody UsuarioDB usuario){

        UsuarioDB resultado = repository.insert(usuario);
        return new ResponseEntity<String>(resultado.toString(),HttpStatus.OK);
    }

    //PUT: Actualiza por usuario
    @PutMapping(value = "/usuarios/{usuario}")
    @ApiOperation(value = "Actualiza un Usuario", notes = "Este método actualiza un Usuario")
    public ResponseEntity<String> actualizarUsuario(@PathVariable String usuario, @RequestBody UsuarioDB usuarioDB){
        Optional<UsuarioDB> resultado = repository.findById(usuario);
        if(resultado.isPresent()){
            UsuarioDB usuarioAModificar = resultado.get();
            usuarioAModificar.estado = usuarioDB.estado;
            usuarioAModificar.idPerfil = usuarioDB.idPerfil;
            usuarioAModificar.correo = usuarioDB.correo;
            UsuarioDB saved = repository.save(resultado.get());
            return new ResponseEntity<String>(resultado.toString(),HttpStatus.OK);
        }
        return new ResponseEntity<String>("Usuario a actualizar no encontrado",HttpStatus.NOT_FOUND);
    }

/*    //PUT: Actualizar todos los usuarios
    @PutMapping(value = "/usuarios")
    public ResponseEntity<List> actualizarEstados(@RequestBody UsuarioDB usuarioDB){

    }*/

    //DELETE: Eliminar todos
    @DeleteMapping(value = "/usuarios")
    @ApiOperation(value = "Eliminar todos los Usuario", notes = "Este método elimina todos los Usuarios")
    public ResponseEntity<String> deleteAllUser(){
        repository.deleteAll();
        return new ResponseEntity<String>("Se han eliminado todos los usuarios", HttpStatus.OK);
    }

    //DELETE: Eliminar por usuario
    @DeleteMapping(value = "/usuarios/{usuario}")
    @ApiOperation(value = "Elimina un Usuario", notes = "Este método elimina un Usuario")
    public ResponseEntity<String> deleteUsuario (@PathVariable String usuario){
        Optional<UsuarioDB> resultado = repository.findById(usuario);
        if (resultado.isPresent()){
            repository.deleteById(usuario);
            return new ResponseEntity<String>("Usuario " + usuario + " eliminado", HttpStatus.OK);
        }
        return new ResponseEntity<String>("Usuario a eliminar no encontrado", HttpStatus.NOT_FOUND);
    }

}
