package com.bbva.techu.controlUsuarios;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ControlUsuariosApplication {

	public static void main(String[] args) {
		SpringApplication.run(ControlUsuariosApplication.class, args);
	}

}
