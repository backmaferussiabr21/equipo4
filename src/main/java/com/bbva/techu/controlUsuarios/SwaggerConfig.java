package com.bbva.techu.controlUsuarios;

import org.springframework.boot.autoconfigure.cassandra.CassandraProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@Configuration
@EnableSwagger2

public class SwaggerConfig {

    @Bean
    public Docket apiDocket() {
        /* Detalle de los métodos de la API*/
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.bbva.techu.controlUsuarios"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(getApiInfo());
    }

    private ApiInfo getApiInfo(){
        return new ApiInfo("API de Usuarios",
                      "API que gestiona Usuarios y sus Perfiles",
                         "1.0",
                 "http://www.equipo4.net",
                  new Contact("Equipo4", "www.equipo4.com","equipo4@correo.com"),
                "LICENCIA Equipo 4",
                "www.licenciaequipo4.com",
                Collections.emptyList());
    }
}
