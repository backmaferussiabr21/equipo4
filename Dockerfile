FROM openjdk:15-jdk-alpine
COPY target/controlUsuarios-0.0.1-SNAPSHOT.jar app.jar
EXPOSE 8081
ENTRYPOINT ["java", "-jar", "/app.jar"]
